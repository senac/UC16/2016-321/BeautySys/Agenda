
package br.com.senac.banco;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAutil {
    
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Sorvete");
    
    
    public static EntityManager getEntityManager(){
        
        try{
            return emf.createEntityManager() ; 
        }catch(Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("Falha ao acessar o banco.");
        }
        
    }
    
    
}
