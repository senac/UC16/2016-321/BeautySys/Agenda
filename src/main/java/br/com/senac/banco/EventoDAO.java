package br.com.senac.banco;

import br.com.senac.modelo.Evento;


public class EventoDAO extends DAO<Evento> {

    public EventoDAO() {
        super(Evento.class);
    }

}
