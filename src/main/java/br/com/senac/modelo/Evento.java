package br.com.senac.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;

@Entity
public class Evento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titulo;
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicio;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fim;
    private String descricao;
    private boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ScheduleEvent toScheduleEvent() {
        DefaultScheduleEvent evt = new DefaultScheduleEvent();
        evt.setId(String.valueOf(this.getId()));
        evt.setStartDate(this.getInicio());
        evt.setEndDate(this.getFim());
        evt.setTitle(this.getTitulo());
        evt.setDescription(this.getDescricao());
        evt.setData(this);
        evt.setEditable(true);
        return evt;
    }
    
    public void toEvento(ScheduleEvent evt){
        this.id = Long.parseLong(evt.getId());
        this.inicio = evt.getStartDate() ; 
        this.fim = evt.getEndDate() ; 
        this.titulo = evt.getTitle() ; 
        this.descricao = evt.getDescription();
    }

}
