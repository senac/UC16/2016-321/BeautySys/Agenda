package br.com.senac.bean;

import br.com.senac.banco.EventoDAO;
import br.com.senac.modelo.Evento;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@ViewScoped
public class EventoBean implements Serializable {

    private static final long serialVersionUID = 8458259708861027697L;

    private Evento evento; // ----> sua entidade de modelo de negocios 
    private List<Evento> listaEvento; // --> Lista de eventos 

    private ScheduleModel eventModel; // Representa o calendario
    private ScheduleEvent event = new DefaultScheduleEvent(); // representa um evento no calendario
    private EventoDAO eDao;

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public void setEventModel(ScheduleModel eventModel) {
        this.eventModel = eventModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    
    private void EventoInicial(){
        this.evento.setTitulo("Raspar suvaco");
        this.evento.setDescricao("Raspar suvaco de fulado ");

        
        this.evento.setInicio(new Date("15/12/2017 08:00:00"));
        this.evento.setFim(new Date("15/12/2017 08:00:00"));

        this.eDao.save(evento);
    }

    @PostConstruct
    public void inicializar() {

        eDao = new EventoDAO();
        evento = new Evento();
        event = new DefaultScheduleEvent();
        eventModel = new DefaultScheduleModel();
        
        this.EventoInicial(); // remover depois --- PU esta como create e Drop

        preencherCalendario();
    }

    private void preencherCalendario() {
        listaEvento = eDao.findAll();

        for (Evento ev : listaEvento) {
            eventModel.addEvent(ev.toScheduleEvent());
        }
    }

    /* eventos do calendario */
    public void addEvent(ActionEvent actionEvent) {

        this.evento.toEvento(event);

        if (this.evento.getId() == 0) {
            eDao.save(evento);
            addMessageInfo("Salvo com sucesso.");
        } else {
            eDao.update(evento);
            addMessageInfo("Alterado com sucesso.");
        }
        event = new DefaultScheduleEvent();
        this.preencherCalendario();
    }

    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }

    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
    }

    private void addMessageInfo(String mensagem) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, mensagem);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
